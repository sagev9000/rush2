use std::{io, io::prelude::*};
use std::path::{Path, PathBuf};
use std::iter::Peekable;
use std::env;
use std::fs;

// Paste code between lines of `%%%` into intermediate.rs
//   And drop these lines for intermediate.sh
// Get function names:
//   if word == "fn", add word.peek() to function_list
// Find their uses in the script:
//   for word in words {if word in function_list {...}}
// Replace them with a form that rush can parse:
//   easily_scared("boo") -> echo "easily_scared" "boo" | .rsh.compiled

fn easily_scared(msg: &str) {
    if msg == "BOO" {
        println!("AAAAAAAHHHHHH!");
    } else {
        println!("Oh, that's fine.");
    }
}

fn output_main() {
    for line in io::stdin().lock().lines() {
        let line = line.unwrap().trim().to_string();
        let (func, args) = line.split_once(" ").unwrap();
        // let args = args.split_whitespace().collect(); // Multiple args.

        if func == "easily_scared" {
            easily_scared(args);
        } else {
            println!("Function not found in `{}`", line);
        }
    }
}

fn is_valid_ident(c: char) -> bool {
    c.is_alphanumeric() || c == '_'
}

fn collect_rust_code<'a>(lines: &'a mut Peekable<std::str::Lines>) -> Vec<&'a str> {
    lines.take_while(|line| !line.starts_with("%%%")).collect()
}

fn collect_rust_fns(lines: Vec<&str>) -> Vec<String> {
    lines.iter().filter(|line| line.trim().starts_with("fn"))
        .map(|line| line.split_whitespace().nth(1).unwrap())
        .map(|func| func.split(|c: char| !is_valid_ident(c)).nth(0))
        .map(|func_name| func_name.unwrap().to_string())
        .collect()
}

#[derive(Debug)]
struct RushFiles {
    // TODO
    // Might be better if these were 
    // references to the respective files
    script_path: PathBuf,
    compiled_script_path: PathBuf,
    intermediate_rust_path:PathBuf,
    compiled_rust_path: PathBuf,
}

impl RushFiles {
    fn new(script_path: &str) -> Self {
        let script_path = PathBuf::from(script_path);
        let filename = script_path.file_name()
            .unwrap().to_str().unwrap().to_string();
        let mut dir = script_path.clone();
        let mut new_buf = |extension| {
            dir.pop();
            dir.push(format!(".{}.{}", filename, extension));
            dir.clone()
        };

        Self {
            script_path,
            compiled_script_path: new_buf("merged.sh"),
            intermediate_rust_path: new_buf("intrmdt.rs"),
            compiled_rust_path: new_buf("compiled"),
        }
    }
}

fn main() {
    if let Some(script_name) = env::args().nth(1) {
        let code = fs::read_to_string(&script_name).unwrap();
        let paths = RushFiles::new(&script_name);

        let mut code_lines = code.lines().peekable();
        if let Some(first_line) = code_lines.peek() {
            if first_line.starts_with("#!") {
                code_lines.next();
            }
        }
        let mut rust_fns = vec![];
        while let Some(line) = code_lines.next() {
            if line.starts_with("%%%") {
                let rust_code = collect_rust_code(&mut code_lines);
                for rust_line in &rust_code {
                    println!("[32m{}[0m", rust_line);
                }
                rust_fns.append(&mut collect_rust_fns(rust_code));
                // println!("[31mfn list:");
                // for rust_fn in &rust_fns {
                //     println!(" * {}", rust_fn);
                // }
                // print!("[0m");
            } else {
                let mut words = line.split_whitespace();
                while let Some(word) = words.next() {
                    if rust_fns.iter().any(|f| word.starts_with(f)) {
                        let (func, args) = word.split_once("(").unwrap();
                        let args = args.rsplit_once(",").unwrap().0;
                        println!("echo \"{}\" {} | {}", func, args, paths.compiled_rust_path.to_str().unwrap());
                        println!("{} \"{}\" {}", paths.compiled_rust_path.to_str().unwrap(), func, args);
                        break;
                    } else {
                        print!("{} ", word);
                    }
                }
                println!();
            }
        }
    }
}
