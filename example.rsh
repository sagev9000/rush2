#!/home/sage/projects/rush2/target/debug/rush2
#!/bin/bash
# /bin/rush2

%%%
fn easily_scared(msg: &str) {
    if msg == "BOO" {
        println!("AAAAAAAHHHHHH!");
    }
}

// echo "boo" | easily_scared
// echo "boo" | ;;easily_scared
// easily_scared("boo");
// difference could correspond to 
// * echo "easily_scared" "BOO" | ....compiled
// * ./....compiled "easily_scared" "BOO"
// Sounds like trouble, though
%%%

echo "BANANA"
easily_scared("BOO", "heyo");
